# Toolbox presentation and working environment (DRISS)

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by
- HackYourPhD (opengeek)
- translation and modification by DRISS
- Updated March 2020</small>


---
## Outlines


0. Intro: bit of history, history of bits
1. bash (taking control of the computer)
2. python terminal (live test of the code)
3. code editor (scripting and documenting)
4. git (project, version and team management)
5. wiki and markdown syntax (documenting the project)

---

# A bit of history - History of bits

#### Automate operations and repetitive process

The vast majority of mankind has no skill in **performing repetitive task with no error**.
And it is painfull and anoying.

![Automatization win](./img/automation-win.png)

Examples :
 - make a lot of addition manual
 - copy a book
 - screw bolts

 ![temps modernes](img/Modern_Times_poster.jpg)

---

#### First automation machines

* Mecanical systems to perform a serie of operations

-87 : Machine d'Anticythère

![ancient analog computer ](img/Machine_Anticythere.jpg)

1645 : Pascaline computer machine of Pascal

![Pascaline](img/pascaline.jpg)

XVIIth century : automation machine craze

![automates](img/Canard_Vaucanson.jpg)

![automates](img/Automates-Jaquet-Droz.jpg)

#### Computer short history

#### Crumbs of theory

**Automated machines** :

* define by a set of states and rules of transition between this states

[Turing machine](https://fr.wikipedia.org/wiki/Machine_de_Turing)
Theoretical object defined by [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing)

> the minimal stuff to implement every kind of program
> inspired by logician : logical gates apparition

**Registry machines**
Everything is explained by [Daniel Dennett](https://en.wikipedia.org/wiki/Daniel_Dennett) [here](http://sites.tufts.edu/rodrego/files/2011/03/Secrets-of-Computer-Power-Revealed-2008.pdf)

#### Principles

* **A deterministic system**
Informatics works well on everything that is regular. Exceptions are and will always be painfull to handle.

* **A minimal set of basic instructions**

No place for ambiguity, simple commands for simple operations

* **Compositionnality**

Learning programmation is like learning music.
Differents levels of integration coexist.

> robot ehymology : robotnik "forced worker" from Czech

#### Electronic implementation
+ analog computers
+ transistors, semi-conductors and micro-electronic

#### Nowdays

**processor** handle 0 and 1, written in memory by following specific instructions.

**memory** of your computer :

 - living memory, short term memory better known as RAM (information is erased when power off)
 - hard disk (information remains as long as not modified)

More on [history of computers](https://www.computerhistory.org/timeline/computers/)

---

## Communicate with a computer

### Computer langages

Computer **langages** informatiques let write information more easily than write down sequences of 0 and 1.

 - A one time, not much better you needed punched cards
  ![cartes perforées](img/Punched_card.jpg)
  be happy with your keyboard, better than a punching machine
 - explosion of programming langages for specific or generic needs

---

You will see in this class

   -  MarkDown
   -  [Python](https://www.python.org/)
   -  [Bash]()

For a given langage, interaction with the computer will be perform at a specific level of integration.
We choose a langage according to the type of problem to solve and also the specific use in your research community. 

----
### Different programming language : 

> Python : data stuff
> Java : data stuff and in private company
> Javascript : web develpmnt
> C, C++, C#
> R : statistics, data 

---

### Why bash?

 + langage of the terminal : interact with your computer
 + *open source*

> Bash : Born Again Shell

---

Why markdown?
 + easy to read easy to write
 + *open source*
 + descriptive language

---

Why python?
  + conceived to be (relatively) easy to read and write
  + *open source*
  + general language: you can do whatever you want with python from web to drones
  + big  community: lot of resources, lot of examples, lot of help, lot of contribution

---

## 1. Bash (taking control of the computer)

The preferred interface with the computer to submit it to our will is the terminal.
Terminal use a language that let you communication with the computer i.e send instructions to the computer by typing the command and see the output of theses instructions shown on the screen.

### Access to the command line

Through the looking glass... the entry point right : inside the head of your computer

![the matrix terminal](https://upload.wikimedia.org/wikipedia/commons/5/53/TheMatrix.png)

system|nom|how to open|language
-------|--------|--------|------
windows|command window,... |win+R puis cmd| dos
mac|terminal,iterm,...|/Applications/Utilities/Terminal| bash
linux|terminal emulator,xterm,shell...|log in, ctrl+alt+t|bash, tcsh, zsh

---

**Windows**

  - Search for cmd.exe or win+R then cmd.
  - Anaconda Prompt: [Anaconda](https://conda.io/miniconda.html) will be your entry point to your computer

---
**Mac**
  - Search from the Spotlight
  - For Mac you can also use [Iterm2](http://iterm2.com/)
  - Anaconda Prompt: [Anaconda](https://conda.io/miniconda.html)

---

**Linux**
  - If you use Linux, you should have some basic skills on command line.
  - Several Terminal available (Ctrl+Alt+T)

> Open a terminal. What are the information displayed automatically?`


---
## Command line and files

Command line makes all the operation that you can perform the a graphical interface, this is quite useful when you have to interact with no screen or have a lot of boring operations to do.

### Hierarchy of files

Information stored in the hard drive is represented as a tree

**Always know where we are in tree** (this is also valid for evolution )

---

### **key concepts** :
- **hierarchy** : as a postal address, to idetify to whom we deliver a document, we go from the top of the scale part and specify then a specific container at each level of organization. For example, we are on earth, in this country, in this peculiar city, in that street, inside this building in this room.
- **root** : the starting point of the hierarchy of information (specificity of Windows :it may have multiple roots)
- **directory** or **folder** : it's a container that can hold files or others folders
- **files** : to simplify it corresponds to a memory zone where are stored informations (text, video, data, script,...)
- **path** : description on where the folder or the file is located starting from the root. It's an adress and like a postal address, it the sequence of the different steps to find the ressource.
- **current folder** also known as *working directory* : the place where we are in the tree at the moment. By default, it is in the current folder that we will try to open or write files if we do not precise the location
- relative path : the path from the current folder
- absolute path: the path from the root (the top of the tree)

---

### Notes :

- we can edit files but not directories!
- files and directories has metadata (as your photos, emails, documents)  that declares special rights and permissions: who and what
- technically a directory is also a file (it's info in memory) but it's not necessary to mixed it up.

---

### Tips:

**very important** : to avoid any problem (especially in command line), always name after your folders and directory  with this rules:
  1. no space
  2. no caracters except:
     - english alphabet (lower and uppercase)
     - arabic numerals
     - the `_` (`-` is also tolerated)
     - the `.` exclusively and with no exception used between a filename and its extension (the few letesr at the end that by convention identify the type of information contained in a file
  3. not the same name of another useful element in your computer


**Very very very important**: But be very careful, when you delete a document it is forever no back up except if you have a control version system for your documents!

---
## Command lines 101

### Basic commands

1) Where are you ?

- `pwd` : where you are (file) (Mac and Linux)
- `chdir` : where you are (Windows)

When you enter to the terminal you will be inside your personal directory which is

- for Mac and Linux
  /home/myusername in Linux and Mac noted by ~

- for Windows
  and C:\Users\NameoftheLaptop in Windows

---

2) Navigation to one file to an other

Same for the different OS

**Warning** with Windows : use  `\` and not `/` for the path
but in your case you will manage it in Unix  style

- `cd` : change directory

> Example : `cd /path to the file we want to access`


- cd `../` : return backward into the parent directory

- cd : go to your personal directory

> Example : `cd`: will place you in your personal directory


- cd Documents/ : got to the Documents directory

<small> Tip use Ctrl+Tab to use autocompletion</small>

---

- `.` : means  : where we are

Example : I'm in the file /root/usr/. I want to got to the file  /root/usr/local/.

> Translation : `cd ./local`

---

- `..` :  file where our file is located

Example : I'm in the file /root/usr/local. I want to got back to /root/usr/.

> Translation : `cd ..`

---

3) What is inside my file ?

- `ls` : list all the files in the repertory l
- `ls -al` : list all the files and give informations on the size, owner, creation date

---

4) Manipulate folders and file

- `mkdir`+ NAME_OF_DIR: make dir, create a directory
- `touch` NAME_OF_THE FILE : create an empty file
- `rm` NAME_OF_THE_FILE : remove a file
- `rm -rf` NAME_OF_DIR: remove the entire directory (recursive folder)
- `cp` copy
- `mv` moove


---

## 2. Python terminal (live test of the code)

### Software installation :

For the course :

- Miniconda

- [Anaconda](http://continuum.io/downloads)
---

### Python 2.0 and 3.0 switch..

We will use Python3.6

### Add-on etc.

  - Jupiter Notebook
  - Atom packages

---

## Warning : Terminal and Python

- Unix Terminal ~

I'm on the terminal
I speak the Shell langage "BASH"
I type `python3`

I'm then in the python interpretor ()>>>)
I will speak in Python
The `>>>` mention the computer waits for instructions.

---

## Other programming software :
<center>

![image](https://img.buzzfeed.com/buzzfeed-static/static/enhanced/webdr03/2013/7/25/5/enhanced-buzz-17903-1374744218-23.jpg?downsize=715:*&output-format=auto&output-quality=auto)

</center>

---

## R vs Python ?

![Data science wars](https://www.orison.biz/blogs/ch3ckmat3/wp-content/uploads/2015/06/r-vs-python-header.png)

[Data Science wars](https://www.orison.biz/blogs/ch3ckmat3/2015/06/r-vs-python-for-data-science/)

---

## 3. Code editor (scripting and documenting)

- Atom.io (basic + packages)

- Other : [Sublime Text](http://www.sublimetext.com), PyCharm.

---

## 4.Git and Gitlab (project, version and team management)

- [Download git](http://git-scm.com/downloads) for Windows / Mac / Linux
- [Download GUI (Graphical User Interface)](http://git-scm.com/downloads/guis) pour Windows / Mac / Linux
- [Git basic principles](git.md)
---

## Github, Gitlab, framagit, etc.

- More information during the next sessions

> [Open geek : version control](https://github.com/HackYourPhd/ateliers-open-geek/blob/master/Archives/Atelier%233.md)

---

## 5. Markdown and wiki (documenting the project)

---
## What is markdown ?

### Présentation

[Markdown](http://daringfireball.net) is :

- a kind of computer language
- easy to learn (compare to LateX or html)
- easy to convert (pandoc power !)
- without the use of complex editor system (and proprietary word etc.)
- you can use it for scholarly work (Atom Package Power ! )  [Scholarly Markdown](http://blog.martinfenner.org/2013/06/17/what-is-scholarly-markdown/)).

---
## Html vs Markdown

Example :

- Italic :
    `<em>exemple</em>` vs  `*exemple*`


<center> But you can combine both in a presentation </center>

---
## First steps

### Titles

    + 1st level : `# My titre`
    + 2nd level : `## My sub-title`
    + 3rd level : `### Mon sub-sub title`
    + etc.

---

### Mise en forme

    + Italic : `*my text*`
    + Bold : `**my text**`

---
### List :

    - first level
        - second level
          - third level

    You can change characters for more visibility

    - first level
        * second level
            + third level

    You can also insert number list

    1. Element 1
    2. Element 2
    3. etc.

---

### Code :

```
A block of code

```
### Comment :

> your comment
> here

---
### Other

- Url link `[Title](http://monlien.com)`
- Image `![Title Image](Image_path.png)`
- etc. (citations, extraits de code source, images, ligne de sÃ©paration, tableaux ...)

---

### Markdown resources

- [OpenClassRooms Tutorial (FR)](http://fr.openclassrooms.com/informatique/cours/redigez-en-markdown)

---
## Other resources

[OpenGeek ressources in french](https://github.com/HackYourPhd/ateliers-open-geek/blob/master/Archives/Atelier%232.md)

---

### Keyboards :

- Mac
  - [ et ] : Alt-Shift-( et Alt-Shift-)
  - { et } : Alt-( et Alt-)

---

> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by HackYourPhD/DRISS (Updated March 2020)
