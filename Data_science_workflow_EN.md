# Data science workflow : an overview

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated Feb 2021</small>

---

## Data science : definition

Data science is :
- interdisciplinar (mathematics, statistics, computer science, information science)
- methodology and management process
- a giant "job market" and a buzzword
- a way of understanding digital society and it's economical, cognitive, political impact.

---


---
###  Data science : new field, methodologies, workflow and impact on our societies

- new field, methodologies and workflow

![Data science](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcLnxOV_AVbTrUXh1mrfcEp-I2_T-iN_Gc1kjgqwHOc5XjMeLk)

> Data science is a "concept to unify statistics, data analysis, machine learning and their related methods" in order to "understand and analyze actual phenomena" with data.[4] It employs techniques and theories drawn from many fields within the context of mathematics, statistics, computer science, and information science.

> The volume covers a wide range of topics and perspectives in the growing field of data science, including theoretical and methodological advances in domains relating to data gathering, classification and clustering, exploratory and multivariate data analysis, and knowledge discovery and seeking. It gives a broad view of the state of the art and is intended for those in the scientific community who either develop new data analysis methods or gather data and use search tools for analyzing and interpreting large and complex data sets. Presenting a wide field of applications, this book is of interest not only to data analysts, mathematicians, and statisticians but also to scientists from many areas and disciplines concerned with complex data: medicine, biology, space science, geoscience, environmental science, infonnation science, image and pattern analysis, economics, statistics, social sciences, psychology, cognitive science, behavioral science, marketing and survey research, data mining, and knowledge organization. (Hayashi et al., Data Science, Classification, and Related Methods, 1996)

Source : [What is Data Science ? Fundamental Concepts and a Heuristic Example Hayashi, Chikio (1996)](https://www.springer.com/fr/book/9784431702085#)

[Data science: how is it different to statistics ? Hadley Wickham](http://bulletin.imstat.org/2014/09/data-science-how-is-it-different-to-statistics%E2%80%89/)


---

## Data science workflow : an overwiew


For Data science :

- **Identify** the source and the format (find the correct information that you want to measure)
- **Acquisition or recollection** : store, clean, format the data (formating diff organizing). Cleaning the data 80% of your time (to deal with the irregularities)
- **Management** : organizing the data
- **Exploration and analysis** : hypothesis (iterative process) : descriptive statistics, modeling, etc.
- **Description** : report, description and documentation of your work
- **Viz** :
- **Publication and communication**


<small> More info : [Ask Quora](https://www.quora.com/Where-do-I-start-and-what-is-step-by-step-process-to-become-a-Data-Scientist) </small>

---

## An iterative process

`It's rare to walk this process in one direction: often your analysis will reveal that you need new or different data, or when presenting results you’ll discover a flaw in your model.` <small>(Hadley Wickham)</small>

---

<center>

![Step](https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Data_visualization_process_v1.png/800px-Data_visualization_process_v1.png)


</center>


---

# Data Collection

---
## What types of data ?
  - Experimental data
  - Databases (survey, repositories, etc.)
  - Logs
  - Articles (scientific, media, etc.)
  - Internet
    - Websites + social network (Twitter, Fb, YouTube, etc.) visible part
    - users' traces (log, etc.) : *analytics*
    - mails
  - etc.

==> different types, formats and ways of collecting and storing data...


> never raw (data is an oxymoron) / datafication

---
## Web & Data : "A beautiful Soup"

<center>

![Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/10.1.jpg)

</center>

<small>[Beautiful Soup Python Package](https://www.crummy.com/software/BeautifulSoup/)</small>

---
## How do we collect data ?

- Databases : private access (Web Of Science), data dump, or *open*  [data.gouv.fr](https://www.data.gouv.fr/fr/) but also collaborative [DBPedia](http://wiki.dbpedia.org/), databases in science (biology, astronomy, physics, etc.)
- Web *scraping* : OpenAPIs or "wild" scraping
- Digitalized Data : Digital Humanities project (OCR optical character recognition)
- etc.

<center>
<small>

[The wild and wonderful world of web scraping](https://medium.com/@spencer.zepelin/the-wild-and-wonderful-world-of-web-scraping-chipy-mentorship-pt-2-6093ef7b11a2)
</center>
</small>

---
## Data collection : several stakes/issues

- Political : data accessibility & rights
- Ethical : personal data, etc.
- Legal: Text and Data mining, privacy regulation, etc.
- Technical: streamed data, storage and organizations

<center>
<small>

Khalil, M., Taraghi, B., & Ebner, M. (2016). [Engaging Learning Analytics in MOOCS: the good, the bad, and the ugly]( https://www.researchgate.net/publication/303913350_Engaging_Learning_Analytics_in_MOOCS_the_good_the_bad_and_the_ugly)

</center>
</small>

---
## Data preparation (1)

> Welcome in the "real-world" datasets !

Clean, explore, organize in a standardized way (tidy data)

- Readings : [Hadley Wickham Tidy Data with R ](http://vita.had.co.nz/papers/tidy-data.pdf) and its version with [Python](http://www.jeannicholashould.com/tidy-data-in-python.html)

- An example with pandas : [Happiness inside a job: a social network analysis](https://www.kaggle.com/harriken/clean-data-python/notebook)

For a dataframe : csv, xls, etc. format

---
## Data preparation (2)

> But the "real-world" is not only dataframes !

Data from the web (following [web scraping](https://www.analyticsvidhya.com/blog/2015/10/beginner-guide-web-scraping-beautiful-soup-python/)) : **a beautiful soup**

You will often deal with JSON format

---
## JSON Format

> JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. It is based on a subset of the JavaScript Programming Language, Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is completely language independent but uses conventions that are familiar to programmers of the C-family of languages, including C, C++, C#, Java, JavaScript, Perl, Python, and many others. These properties make JSON an ideal data-interchange language.

Readings : [JSON format and Python](https://code.tutsplus.com/tutorials/how-to-work-with-json-data-using-python--cms-25758)


---

<center>

![Json-Jason](https://cdn-images-1.medium.com/max/1200/1*ePjYHYK_vw3dGzfeARHxRA.jpeg)
<center>

---

# Data analysis

Welcome to the world of computational research (statistical modeling, machine learning, etc.)


![computational research-jupyter-notebook](https://journals.plos.org/ploscompbiol/article/figure/image?size=large&id=info:doi/10.1371/journal.pcbi.1007007.g001)

> Source : Rule A. et al., 2019, « Ten simple rules for writing and sharing computational analyses in Jupyter Notebooks », PLOS Computational Biology, 15, 7, p. e1007007.

---

## Narratives and Data visualisation

### Importance of documentation and reproductibility

> Rule 1: Tell a story for an audience
One key benefit of using Jupyter Notebooks is being able to interleave explanatory text with code and results to create a computational narrative [7]. Rather than only keep sporadic notes, use explanatory text to tell a compelling story that has a beginning that introduces the topic, a middle that describes your steps, and an end that interprets the results. Describe not just what you did but why you did it, how the steps are connected, and what it all means. It is okay for your story to change over time, especially as your analysis evolves, but be sure to start documenting your thoughts and process as early as possible. (Rule and al. 2019)


---

### The importance of organizing your workflow and think about it event if it is evolving


- [The data science workflow](https://towardsdatascience.com/the-data-science-workflow-43859db0415)
- [Structure and automated workflow](https://towardsdatascience.com/structure-and-automated-workflow-for-a-machine-learning-project-2fa30d661c1e)
- [Cookiecutter Data science](https://drivendata.github.io/cookiecutter-data-science/)
- [Livre blanc sur les données au CNRS. Etat des lieux et pratiques. CNRS, janvier 2018.](http://www.cocin.cnrs.fr/IMG/pdf/livre_blanc_donne_es_2018.pdf)
- Rule A. et al., 2019, « Ten simple rules for writing and sharing computational analyses in Jupyter Notebooks », PLOS Computational Biology, 15, 7, p. e1007007.

### The importance of quality of data

Data will be nothing without : 
- metadata
- open format
- licences 
- standart
- openess  
==> FAIR principles

---

## Data science : not only in research 

- data labour and platformization : Business and new digital economy  

![Business](https://blogs.sap.com/wp-content/uploads/2019/07/Data-Science.png)



---
## Data science : a research field evolving

An introduction with the [College de France course](https://www.college-de-france.fr/site/en-stephane-mallat/index.htm)

---

## Instructions 

Instruction for the afternoon

- Create a file in your repository `2021_Description_project.md`
- Describe your resarch project : - Work on your research project : how to describe it, your methodologies the data..
  - data collection
  - methodology
  - data workflow
  - working environment which tools do you use
  - results how do you share it ?
- Explanation for your working project

- Enigma : Why Jupyter ? 

- ask your question and need. Cours à la carte

## Ressources 

Vos, Rutger, and Pedro Fernandes. 2017. Open Science Open Data Open Source. 21th Century Research Skills for Life Sciences. Zenodo. https://doi.org/10.5281/ZENODO.1015288.

Bednarski, Mateusz. 2017. “Structure and Automated Workflow for a Machine Learning Project — Part 1.” Towards Data Science. May 20, 2017. https://towardsdatascience.com/structure-and-automated-workflow-for-a-machine-learning-project-2fa30d661c1e.

Boechat, Marina, and Tommaso Venturini. 2016. “From Analysis to Presentation.” Les Cahiers Du Numérique 12 (4): 185–204.
Chen, James. 2019. “The Power of Visualization in Data Science.” Towards Data Science. May 16, 2019. https://towardsdatascience.com/the-power-of-visualization-in-data-science-1995d56e4208.

Jupyter-Guide/Ten-Rules-Jupyter. (2018) 2019. Jupyter Notebook. jupyter-guide. https://github.com/jupyter-guide/ten-rules-jupyter.

Konstantin. 2018. “The Data Science Workflow.” Towards Data Science. November 29, 2018. https://towardsdatascience.com/the-data-science-workflow-43859db0415.

Wickham, Hadley. 2014. “Data Science: How Is It Different to Statistics ?” IMS Bulletin (blog). 2014. http://bulletin.imstat.org/2014/09/data-science-how-is-it-different-to-statistics%e2%80%89/.




> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS (Updated Feb 2021)
