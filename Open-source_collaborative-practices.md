# Behind Framagit platform : from collaborative methodologies in software dev to open and data science practices

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated Feb 2021</small>


---

##  Your Answers to the questions :

- What is framagit : how is it organized ? What are the features ? How the files are organized ? What is it used for ?
- What is the language used inside the file .md ?
- Bonus : Why the password of GatherTown is BudapestDeclaration ?

[Let's try markdown](https://codimd.inno3.cricket/jy5IH27dSPatJ6MJpdV3PQ?view)

---

## When the myth becomes reality 

### History of the Internet and Science/Technologies imaginaries :

![History of the internet](https://www.internetsociety.org/wp-content/uploads/2017/09/BriefHistoryInternetTimeline.gif)

> Life Noggin. (2014). HISTORY OF THE INTERNET. Consulté à l’adresse https://www.youtube.com/watch?v=h8K49dD52WA

---

![Lo and behold](https://sawyer-studios.com/wp-content/uploads/2017/02/LO-AND-BEHOLD.jpg)

> Lo and behold trailer https://www.youtube.com/watch?v=Zc1tZ8JsZvg

---


### Digital technologies and research : entanglement


![Tim Berners Lee](https://i0.wp.com/marketbusinessnews.com/wp-content/uploads/2017/06/World-Wide-Web-Tim-Berners-Lee.jpg?resize=440%2C400&ssl=1)

=> *The ideal of science (ethos) embedded in our technologies*

---
### Ethos in Science : the ideal scientific community

![Merton](https://s1.qwant.com/thumbr/0x0/8/8/70effa6b01638b0ce5a110a7681ec33e10c1b2c5cae397c466da02fd787127/hs-220-12-638.jpg?u=https%3A%2F%2Fimage.slidesharecdn.com%2Fhs220-131028093617-phpapp01%2F95%2Fhs-220-12-638.jpg%3Fcb%3D1382953053&q=0&b=1&p=0&a=1)

> C : Communalism
> U : Universalism
> D : Desinterst
> OS : Organized Scepticism


---

## A concretisation :  Free libre and Open Source Softwares

Free Libre Open Source Softwares
- methodologies
- business model
- ideologies
- organizations

----

## Free software : the four essential freedoms

A program is free software if the program's users have the four essential freedoms: [1]

- The freedom to run the program as you wish, for any purpose (freedom 0)
- The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
- The freedom to redistribute copies so you can help others (freedom 2).
- The freedom to distribute copies of your modified versions to others (freedom 3) By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

> Def [freesoftware](https://www.gnu.org/philosophy/free-sw.en.html)

---

## Open Source methodologies : the bazaar and the cathedral

![Open source principles](https://img.libquotes.com/pic-quotes/v1/linus-torvalds-quote-lbh3i7p.jpg)

Release early, release often

---
## Agile Manifesto
<center>

![Agile Manifesto](https://www.stickyminds.com/sites/default/files/article/2017/AgileManifesto.png)

</center>

---
## Behind Framagit platform : Gitlab

Software development platforms :
- based on version control system (Git)
- manage large project
- work with a team in a collaborative way
- based on agile method

[Gitlab](https://about.gitlab.com/)


---
## Git

![Git](https://ds6br8f5qp1u2.cloudfront.net/blog/wp-content/uploads/2016/05/gitlab-vs-github-1024x358.png?x88475)

> Watch the [Tedtalk with Linus Torvald](https://www.youtube.com/watch?v=Vo9KPk-gqKk)
> cf. Git_basics.md

---
<center>

![Git meme](https://bluecanvas.io/images/git-memes/git-commit-organized.jpg)

</center>

--- 

### Open source : business models 

- services vs proprietary licences
- open models : work in an ecosystem. Do not possess but be one of the leader of the use
- hack the Intelectual property 

---
###  Critical perspectives : open versus open

![Git_Gitlab_Github](http://www.amarinfotech.com/wp-content/uploads/2017/05/GitLab-vs-GitHub-vs-bitbucket-1.jpg)

<center>

[The Key Differences : GitLab vs GitHub vs bitbucket](http://www.amarinfotech.com/gitlab-vs-github-vs-bitbucket.html)
</center>

==> *Plaformization everywhere : centralization of power even if it's look like a network*

---

## What Open and Data science should learn from Open Source communities ? (Good practices) 

---

### Organize your working environment

> cf. Toolbox_Working_environment.md

---

### Organize your files and documents 

![xkcd](https://imgs.xkcd.com/comics/disk_usage.png)

> Rivet Alain, Bachèlerie Marie-Laure, Denis-Meyere Aurianne, and Tisserand Delphine. 2018. “Réseau Qualité En Recherche - Traçabilité Des Activités de Recherche et Gestion Des Connaissances - Guide Pratique de Mise En Place.” Mission pour les Initiatives Transverses et Interdisciplinaires. http://qualite-en-recherche.cnrs.fr/spip.php?article315.

---

### Document and keep track of your work

![Version control](https://cdn-images-1.medium.com/max/1600/1*qEUqbZM-H39_qOxwTIV7Dw.jpeg)

---

### Ask Google but...

![Ask Google](https://c1.staticflickr.com/4/3469/3381634070_6818761977.jpg)


---

### Search engines and results

Keep in mind that search engines are not neutral...

![Search result](https://i.vimeocdn.com/filter/overlay?src0=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F352723558_1280x720.jpg&src1=https%3A%2F%2Ff.vimeocdn.com%2Fimages_v6%2Fshare%2Fplay_icon_overlay.png)

==> *Google Scholar and harvesting : quite scary...*

--- 

### Ask help from the communities

It's real people, I can assure you :) 

---

## Reversed classroom and peer learning
<center>

![Flipped classroom](http://texascomputerscience.weebly.com/uploads/3/2/1/0/32104975/778970002.gif?512)

</center>

- Active learning
- Peer Interaction/learning
- More personalized session
- Real "job life" immersion


> <small> For a better understanding, look at this presentation  [ Flipped Classroom Model and Its Implementation in a Computer Programming Course](http://www2.bth.se/bib/lararlardom.nsf/bilagor/eric_chen_pdf/$file/eric_chen.pdf)
</small>

---

## Open source culture and Research : mind the gap 

and find solutions 

[FLOSS meets social sciences](
https://validate.video.fosdem.org/30eae5b61ce1eb9808d7fdea589cebd76da73229a611d9f56a9c68dbc28f79a2.mp4)

----

## Enigma : before the lunch

What are the meaning of the pictogram at the beginning of each of this presentation ? 

---


## Ressources 

FOSDEM 2021. n.d. “Open Research Tools and Technologies Devroom.” Accessed February 7, 2021. https://fosdem.org/2021/schedule/track/open_research_tools_and_technologies/.

Vos, Rutger, and Pedro Fernandes. 2017. Open Science Open Data Open Source. 21th Century Research Skills for Life Sciences. Zenodo. https://doi.org/10.5281/ZENODO.1015288.

Gruson-Daniel Célya, Jean Benjamin. 2020. Etude relative à l'ouverture des codes sources en recherche : étude en termes d'usage et de valorisation. https://opensource-esr.pubpub.org/




