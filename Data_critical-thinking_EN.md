# Data science : historical, socio-political and economical perspectives

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated February 2020</small>

---

## Data

> "The very term ‘data’ comes from the Latin ‘given’, and indeed data are meant to document as faithfully and objectively as possible whatever entities or processes are being investigated."`<small>(Leonelli, Bezuidenhout 2015)</small>

---
# A data deluge ?

![data deluge](https://www.maxpixel.net/static/photo/640/World-Cloud-Information-Big-Data-Data-Global-1667184.jpg)

---
# A bit of "déjà-vu"...

`Every age was an age of information, each in its own way`
<small> (Darnton, 2000)</small>

Watch : Amplification and Digital Technologies

> Dominique Boullier. (2016). Enjeux socio-politiques du numérique Saison 1 Episode 1 Amplification. Consulté à l’adresse https://www.youtube.com/watch?v=gfsv5df8SYw

### The age of Data

#### Amplification

![Amplification](https://i.ytimg.com/vi/gfsv5df8SYw/maxresdefault.jpg)

> Dominique Boullier. (2016). Enjeux socio-politiques du numérique Saison 1 Episode 1 Amplification. Consulté à l’adresse https://www.youtube.com/watch?v=gfsv5df8SYw

---

### New digital economy and professional skills : data science and IA 

- Digital Labor 
- Data scientist, ML engineer in research but especially in the companies 

> Vos, Rutger, and Pedro Fernandes. 2017. Open Science Open Data Open Source. 21th Century Research Skills for Life Sciences. Zenodo. https://doi.org/10.5281/ZENODO.1015288.

---
# Data, science and power

**Science** :

- A discourse of truth : knowledge/power
- A way of describing the "reality"
- A performative effect

**And data have a major role to play**

`science that is most explicitly and exclusively devoted to the
ordering of complex data` <small>(Simpson, 1961)</small>

---

# Natural Sciences and classification

<center>Classification Systema Naturae (1735)</center>

![classification Systema Naturae Linnée 1735]( https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Linnaeus_-_Regnum_Animale_%281735%29.png/1280px-Linnaeus_-_Regnum_Animale_%281735%29.png)

---

# An impact on social theories

Political arithmetics and social physics <small>(Dostaler 2009)</small>

![social physics Pentland](https://c1.staticflickr.com/3/2925/14080899362_5820aed06c_b.jpg)

---
# Science and governance : the power of numbers

![Diagram of the causes of mortality in the army in the East" by Florence Nightingale.](https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Nightingale-mortality.jpg/800px-Nightingale-mortality.jpg)

---
# Science, statistics and State

Science and decision process : the rise of statistical analysis

`To require specific kinds of data or to even insist on using data-driven decision-making is to ensure that the “right” decisions are made by the “right” people.`
<small> (Robertson, Travaglia 2015)</small>

<small> Must read : Desrosières, A. (2014). Statistics and social critique. Partecipazione e conflitto, 7(2), 348–359. https://doi.org/10.1285/i20356609v7i2p348 </small>

---
## Encoding and decoding data : never neutral

- dominant social order
- a view about society
- impose a meaning

`Data does not just allocate material things of value, it allocates moral values as well.`<small>(Robertson, Travaglia 2015)</small>

Science and a matter of fact

https://category-theory.mitpress.mit.edu/books/matter-facts

---

## Reflexivity : roundtable

Data and covid-19

---

# Data science : actual socio-political and economical stakes

---

## The age of technosciences : uncertainty, controversies and its limits

Uncertainty, simulation, controversies

![FORCCAST project](https://f.hypotheses.org/wp-content/blogs.dir/1141/files/2013/05/Forccast-logo.png)

<center> http://enforccast.hypotheses.org/

## Amplification of data-driven decision making



----
## Covid-19 data and public policy

- data everywhere : dashboard in all presentation, justification
- Trust in science ?
- Data and narratives : matter of evidence

But also fake news, opinion bubbles

https://www.lse.ac.uk/Events/COVID-19-The-Policy-Response
Bernadette Bensaude Vincent

---
## Data and narratives : matter of evidence

https://www.washingtonpost.com/graphics/2020/world/corona-simulator/

---
## Data and counter-power

- Data co-production
- Openness (*open data, open gov*)
- Transparency and reproducibility(*open science*)

---

## Data science and Discourse : Machine learning, IA <-> Digital Bullshit bingo !

- Digital Labor and cognitive capitalism
- Business model and data

![Calling Bullshit](http://www.callingbullshit.org/twittercards/index.png)

> Carl Bergstrom, & Jevin West. (s. d.). Calling Bullshit:  Data Reasoning in a Digital World. Consulté 24 janvier 2019, à l’adresse https://callingbullshit.org/


----

### Data shaping and algorithms

- Look at this TED ["How algorithm shape our world"](https://www.ted.com/talks/kevin_slavin_how_algorithms_shape_our_world) to understand the influence of algorithms in governance today.
- And a lot of [ressources](https://socialmediacollective.org/reading-lists/critical-algorithm-studies/) here

---

### Inequalities perspectives

- Women's Programming :

History of Computer Science : Anecdote: A Grace Hopper and a Bug
![The December 9th, 2013 Google Doodle Honors Grace Hopper's 107th Birthday...and has an entomological surprise at the end. Image © Google.](https://www.google.com/logos/doodles/2013/grace-hoppers-107th-birthday-5447077240766464.4-hp.gif)

Source [wired](https://www.wired.com/2013/12/googles-doodle-honors-grace-hopper-and-entomology/)

- AI and gender and raciel bias

Source [Time](https://time.com/5520558/artificial-intelligence-racial-gender-bias/)

---

### Data for goods


![Hypocrate](https://hippocrate.tech/static/share_image.png)


[Serment d’Hippocrate pour data scientist](https://www.hippocrate.tech)

----

#### An exemple : Data and algorithm activism

![school of data](http://www.cameralibre.cc/wp-content/uploads/SCODAbadges.png)

[Stactactivisme](https://editionsladecouverte.fr/catalogue/index-Statactivisme-9782355220548.html)

[Algotransparency](https://algotransparency.org/index.html)

> We aim to inform citizens on the mechanisms behind the algorithms that determine and shape our access to information. YouTube is the first platform on which we’ve conducted the experiment. We are currently developing tools for other platforms.

---

### Data science and digital labour

<center>
![Immaterial labour](https://labs.rs/wp-content/uploads/2016/08/FB-Research-Final-02.png)
</center>

[Facebook algorithm and immaterial labour](https://labs.rs/en/facebook-algorithmic-factory-immaterial-labour-and-data-harvesting/)

---

## Critical perspectives : business models of openess / cognitive capitalism /

Gruson-Daniel Célya (2019), « An open drifting away? Economic models of open and (informational) liberalism », https://phd-cgd.pubpub.org/pub/open-liberalism-en.
