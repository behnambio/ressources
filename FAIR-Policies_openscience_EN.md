# Open Science guidelines and policies : be FAIR and DMPed

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated Fev2020</small>

----
## Open Science : a few years ago...

![HackYourPhD](https://hackyourphd.org/wp-content/uploads/2013/11/header3.jpg)

----
## Open Science today

![EOSC](https://eoscpilot.eu/sites/default/files/eosc_eoscpilot_rs.jpg)

---

### Open science : mutiple vision (definition, aims, etc.)

![Elephant CCBYSA romana klee](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Medieval_Jain_temple_Anekantavada_doctrine_artwork.jpg/1280px-Medieval_Jain_temple_Anekantavada_doctrine_artwork.jpg)

- answer to an epistemic crisis and reproducibility advocacy
- infrastructures and tech to deploy/adapt for an efficient research in a digital world
- new business model and legal issues in the research system
- new governance and organization for research and interaction with the society (commons)

---

## Today : new norms and (mandatory) practices

---
## From Open Science to FAIR principles

- To answer to a "broken science or research system"

Randall D. et al., 2018, [The irreproducibility crisis of modern science: causes, consequences, and the road to reform](https://www.nas.org/reports/the-irreproducibility-crisis-of-modern-science).
Research of Research...

- To organize, create international norms and infrastructures in all research fields

> Findable, Accessible, Interoperable, Reusable

![Fair principles wikipedia](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/FAIR_data_principles.jpg/800px-FAIR_data_principles.jpg)

> Open Standard, specific repositories, metadata, ontologies, archive and perenity, DOI (Digital Object Identifier)

----

## Data management plan : before all research project

- Europe Science, « Practical Guide to the International Alignment of Research Data Management », Tipik Communication Agency, https://www.scienceeurope.org/our-resources/practical-guide-to-the-international-alignment-of-research-data-management/.

- « L’ANR met en place un plan de gestion des données pour les projets financés dès 2019 », Agence nationale de la recherche, https://anr.fr/fr/actualites-de-lanr/details/news/lanr-met-en-place-un-plan-de-gestion-des-donnees-pour-les-projets-finances-des-2019/

Some examples of DMP in [opidor platform](https://dmp.opidor.fr/public_templates)

----
## Open Access and Plan S

- Open access policies in your institute : put you article in an institutional archives
- Plan S : fight against "double pay", add a licence to your content (licence CC-BY)

---

## Open source, open data and open science

- Open Data, open source for IA (geopolitical, quality, economy)
- open source and open data for reproducibility, transparency

---

## Open science and decolonization of knowledge

- knowledge/power : heterogeneity vs. bibliodiversity
- legitimity of other knowledge : experiental, traditional, etc.

> Chan, Leslie, Budd Hall, Florence Piron, Rajesh Tandon, and Wanósts’a7 Lorna Williams. 2020. “Open Science Beyond Open Access: For and with Communities,  A Step towards the Decolonization of Knowledge,” July. https://doi.org/10.5281/zenodo.3946773.
https://plus.google.com/+UNESCO. 

> 2020. “UNESCO Recommendation on Open Science.” UNESCO. March 2, 2020. https://en.unesco.org/science-sustainable-future/open-science/recommendation.

### Open Source,  Commons and Open Science 

- for reproducibility
- for sustainability 
- for change in governance 
- for an other way of practicing research 

### Open versus Open ?

Gruson-Daniel Célya, Numérique et régime français des savoirs en~action : l’open en sciences.  Le cas de la consultation République numérique (2015),Université Paris Descartes, 2018.

[An open drifting away? Economic models of open and (informational) liberalism](https://phd-cgd.pubpub.org/pub/open-liberalism-en)

---
## Keep in mind

- Research workflow
- Use of repositories (not your website or dropbox or only private research network)
- Importance of DOI (ORCID)
- Journal in open access or Repositories (and now preprint)
- Data and Software can also have research credit
- Legal and ethical considerations
- Society

----




> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS (Updated Feb 2020)
