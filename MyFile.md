<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by Behnam
- Updated Feb2021 </small>  

### Modeling of Systemic Autoimmune Disease based on Gene Expression.  

## Introduction
<small> Systemic autoimmune disease (SAD) develops as a loss of self-tolerance to ubiquitous autoantigens at the systemic level. The resulting diseases are heterogeneous and vary significantly among patients. Unfortunately, such diseases are so complex that not only no single treatment has shown to be curative.

![Alt text](https://d2jx2rerrg6sh3.cloudfront.net/image-handler/ts/20181113105054/ri/673/picture/2018/11/By_Scio21.jpg)

## Resources
- A number of papers related to my work can be found in:
    - https://www.sciencedirect.com/science/article/pii/S1476927119302130
    - https://pubmed.ncbi.nlm.nih.gov/27040498/
    - https://pubmed.ncbi.nlm.nih.gov/28031441/  
.  
- I use Gene Expression Omninus for the microaray data:
    - https://www.ncbi.nlm.nih.gov/geo/

## Pipeline
<small> In this project, we aim to develop computational approaches in the context of SADs for identifying the (possibly heterogeneous) underlying associated pathways. To meet this end, we will use/develop dimensionality reduction techniques that map available transcriptome profiles into a common and semantically rich low-dimensional space. Dimensionality reduction is to map the input features space to a lower dimensional space using linear or nonlinear transformations. This can be achieved by gene module detection techniques to cluster together the related genes (e.g. based on co-expression networks). Based on this reduced dimensionality space, we will propose a map called Autoimmune map, which brings about new opportunities to better understand the mechanisms underlying SADs, and to stratify autoimmune patients according to their different molecular subtypes (Figure 1).

![Alt text](https://i.imgur.com/wyFKe3m.png "Figure 1")

I Use R Stoudio 

![Alt text](https://datascienceineducation.com/man/figures/Figure%205.1.png "Figure 1")


## Results
<small> Each axes of the Autoimmune map potentially represents the activity of a set of pathways. Figure 2 shows a typical autoimmune map for SLE. Once the transcriptomes of SAD patients have been projected into a low-dimensional space, data samples can be stratified into sub-populations in order to better understand the biology of heterogeneity. It is worth noting that this patient stratification is at the molecular level and may be independent of the disease symptoms and phenotypes. Furthermore, one can test for possible associations of the low-dimensional points with genotypes, which requires additional data recordings. The understanding of patient heterogeneity may lead to tailored treatments, and increase the quality of life of SAD patients. 

![Alt text](https://i.imgur.com/g4lzPCv.png "Figure 2")

## How Make My Project Open?
- Journals
    - Publish in "opne" Journals
    - Publish in HAL and TEL
- Software
    - Make my codes avilabe in GitHub
    - Build free R Package and share them in CRAN
 

