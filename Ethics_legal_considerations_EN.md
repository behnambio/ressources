# Ethics and legal considerations for your research project

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated March 2020</small>

---

## Legal framework with Open Science and Open Data

[Open Data Directive](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32019L1024&from=EN#d1e1505-56-1) (PSI 2 in 2019) :
- article 10 (research data and FAIR principles)

Article 10

Research data 

> 1.   Member States shall support the availability of research data by adopting national policies and relevant actions aiming at making publicly funded research data openly available (‘open access policies’), following the principle of ‘open by default’ and compatible with the FAIR principles. In that context, concerns relating to intellectual property rights, personal data protection and confidentiality, security and legitimate commercial interests, shall be taken into account in accordance with the principle of ‘as open as possible, as closed as necessary’. Those open access policies shall be addressed to research performing organisations and research funding organisations.

> 2.   Without prejudice to point (c) of Article 1(2), research data shall be re-usable for commercial or non-commercial purposes in accordance with Chapters III and IV, insofar as they are publicly funded and researchers, research performing organisations or research funding organisations have already made them publicly available through an institutional or subject-based repository. In that context, legitimate commercial interests, knowledge transfer activities and pre-existing intellectual property rights shall be taken into account.

Digital Republic Law in France (in 2016 ):
- article 30 : open access and open research data
- other article about open data set by default and open source !

----
## Intelectual Property

Your work owns to your employer (exception for researchers.)

---
## A big important part : Personal Data and Health Data

---


## What to do and where to start ?

As a researcher :

- 1/ Know who is the **referee** in your institute (legal department of what we call now the DPO Data Protection Officer)
- 2/ Explain your research and delimit its perimeters to know if you are collecting **personal data/ sensitive data/ health data**.
    - The health data are the most sensitive and neet specific procedures with the CNIL
- 3/ Write a document where you **define in details** :
    - Aims of your research
    - Which data do you want to collect ? Do you need all these data to answer to your research questions (Data minimization)
    - How long will you store the data ? Will you delete it after or ask for a institutional archiving ?
    - How will you inform the participants before the beginning of your research?
    - How your participants can exercise their data protection rights?
    - Do you have designed a secure environment to preserve the data security (storage)
==> cf Data management plan
- 4/ Depending on the data and the perimeters of your research, you will have to **apply for different proposals** to get specific permission:
    - IRB proposal for the ethical commitee agreeement (mandated in the USA for example)
    - Authorization request to the CNIL (Commission Nationale Informatique & Libertés) or to the INDS (Institut National des Données de Santé in France
- You will need maybe **to register your project or sign a commitment** to compliance (MR-001/002 etc. for health data)
- 5/ When everything is ok, stay cautious and use the principles of "ethics by design" and don't forget to eras the data after the expiration of the deadline

Comments : for PhD Students, you can't apply alone to these procedures. Your supervisor or other specific officer will have to apply for it.

> For more informations you can look at this ressources :
> - [Recherche médicale : comment procéder pour une thèse ou un mémoire](https://www.cnil.fr/fr/recherche-medicale-comment-proceder-pour-une-these-ou-un-memoire)
> - [Protocol Guideliens for an IRB proposals](https://sterlingirb.com/protocol-guidelines/)
> - [Données de santé : quelle protection](https://figshare.com/articles/Donn_es_de_sant_quelles_protections_/1601882)

---

### Ethics/Privacy by Design/Ethics of Care

- Even if in research you will have to follow specific rules to be in conformity with **Ethical, Legal and Social Implications (ELSI)**.
- It does not prevent you to adopt a specific attitude/mindset towards what you are doing (reflexivity on your work and research practices).
- Some approaches as ethics/privacy by design, or ethics of care give us specific highlight to act with cautious along your research

> You will often use digital services to collect data. This guideline about [Ethical Decision-Making and Internet Research](https://aoir.org/reports/ethics2.pdf) might be inspiring.


---

## What's new with the GDPR (General Data Protection Regulation) in Europe?

### WHat is the GDPR?

- General Data Protection Regulation or Régulation Générale de la Protection des Données (RGPD)
- A regulation in EU law on data protection and privacy for all individuals within the European Union (EU) and the European Economic Area (EEA).
- It has been implemented on May 2018.
- Some principles : be specific (purpose limitation)/Data minimization/Storage limitations/Integrity and confidentiality/Accountabilty(record and prove compliances)

> Source : [GDPR Info](https://gdpr-info.eu/)

---

### What does it change for scientific research

- This GDPR reflects "good practices" that are already applied in research (informed consent, data minimization etc. )
- Accountability allows to simplify specific procedures
- Now there is a Data Protection Officers in the public university
- It higlights the importance of data minimization
- There are specific derogations :
    - Aims of the project : indetermination of the research possible
    - storage can be longer than the purpose of the research : possibility to archive your data


However, for sensitive data/health data, there are still specific reglementations.

----

## Work in a secure environment : some tips for your passwords

- Secure your password and your session (lock your computer !)
- Renew your password (use an algorithm to generate it)
- Use password manager (a master password to manage different password easily)
- Discover criptography

- Better to have **https**
- For your server, use **ssh-keys**

> Source in french : [Guide de la sécurité personnelle](https://www.cnil.fr/sites/default/files/atoms/files/cnil_guide_securite_personnelle.pdf)

---

## Creative commons licences 

