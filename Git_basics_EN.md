# Git 101

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated March 2020 </small>

---
## Git

![Git](https://ds6br8f5qp1u2.cloudfront.net/blog/wp-content/uploads/2016/05/gitlab-vs-github-1024x358.png?x88475)

> Watch the [Tedtalk with Linus Torvald](https://www.youtube.com/watch?v=Vo9KPk-gqKk)
> cf. Git_basics.md

---
<center>

![Git meme](https://bluecanvas.io/images/git-memes/git-commit-organized.jpg)

</center>

---

The basics:

- The aim of Git is to **manage software development projects** and its files, as they are changing over time.
- A git repository is a central place where developers store, share, test and collaborate on projects.

- Git stores this information in a data structure called a **repository**.
- Such a git repository contains a set of **commit objects** and a set of references to commit objects.

---

## Version control system

2 big systems exists:
* SVN (Subversion)
* GIT

Others are based on Svn (CVS, Perforce, Bazaar, Mercurial)

On the top of it, there are a lot of web platform :

- [bitbucket](https://bitbucket.org/)
- [source forge](https://sourceforge.net/)
- [gitlab](https://about.gitlab.com/)
- [github](https://github.com/)
- [dvc](https://dvc.org/)

### Why?

Software forge:

* Multiple changes in document
* Multiple workers team
* Choose to accept commit or not (pending request)
* Keeping the memory of the changes
* Integrity checks of diff
* Reverting features

### Audience

#### Tech Team

* Big community
* Big projects
* Developpers with the needs of specific feature
* IT assistance
* Debug, test and deploy
* Agile methodology with short cycle of releases

Example : [Linux](https://github.com/torvalds/linux) Look a the number of contributors !

#### Academic

* Keep tracks to the history of documents
* Test and deploy
* Comments from commnunity
* Publish algorithms and datasets : reproductibility
* Peer reviewing
* Visibility of your research

Examples :

- [Research and Education](https://github.com/quantopian/research_public)
- [Neuroscience and genetics : extrospection](https://github.com/deep-introspection/)
- [Political sciences and network analysis](https://github.com/briatte)
- [Hackathon and Data sprint in research](https://framagit.org/c24b/republique-numerique)
- [Social Network Analysis and STS : C24B and celyagd](https://github.com/c24b/openscience4S)

### Principles

* Git is a local registry (database) that stores snapshots of your filesystem in a stream.
(Not the files and the delta change - as SVN subversion - between files but the metadata of your directory)

* If nothing has changed, Git let the previous version.
* If something has changed Git make a checksum of the changes that give an unique id

You don't necessarily need a server you can keep it in local BUT if you want a backup better to keep it in another place

Git may then act as Google drive or a dropbox keeping your data synchronized in another place (*cloud computing*).

#### Vocabulary:

 - *Directory* are called `repository`
 - *Changes* are called `commits`
 - *Message* and comment are called `log`
 - *Status* displays what is watched by git and what is not: it is the equivalent of the green icon in dropbox that show the file is synchronized

## Git commands

3 main commands in the command line :

### Add

* `git add _<your_file>_`

Add your file to the registry of git: i.e store the metadata into the git database the `staging` state
you can see what's in and out git knowledge by using `git status`

### Commit

* `git commit -m "Your commit message" `

Take a snapshot of the system and add log or attach a message to the changes. You can see all the message and the history by using `git log`

### Push

* `git push`

Send it to the remote server if you have one.
You set the default direction the first time with the command `git push -u origin master`
and now it's pushing to upstream origin direction in the master branch
You can see where it is sent by using `git remote -v`

---
**EVEN in a RUSH, REMEMBER to PUSH!**
---

Other additionnal commands (ADVANCED)

## Clone, fork, branch

`git clone` : clone a repository from a server
`git pull`: pull in local what is in the server, reverse of `git push`

`git fork`: clone a repository and create your own version

`git branch`: diverges the code

### Conflicts management

Solve the conflict which versions I should use ?
    - merge
    - rebase

### Fork

One of the main aim of Git was to create different branch :
    - To test your own change and later you can merge it to the master branch (integration of your branch while dealing with conflicts)

### Checkout

Switch to branch


### Anatomy of a commit

Stored a little folder called `.git`
each commit is unique thanks to its ID '197ed3716d15f3718e4a2bee34faa4db719dd7a9'


```
commit 197ed3716d15f3718e4a2bee34faa4db719dd7a9
Author: c24b <4barbes@gmail.com>
Date:   Tue Oct 1 11:49:10 2019 +0200

    Insert gp Values #bug-201
```

With this id you can access to information

`git diff -- 197ed3716d15f3718e4a2bee34faa4db719dd7a9`

## Git plateforms

Github, Gilab, Bitbucket, Forge are social network platforms that includes more than a software forge with git:

* Interactive representation of git
* A markdown editor/previewer
* A wiki
* A text editor
* A static website
* A system to automatically test and deploy (CD/CI)
* To do list
* Tickets or issues
* Comments


## Ressources

* An other ressource made by the HackYourPhD Community (in French) : [Collaboration et suivi des développements]( https://github.com/HackYourPhd/ateliers-open-geek/blob/2014_2015/Atelier%233.md)
* [Git Handbook by Github](https://guides.github.com/introduction/git-handbook/)


> Licence CC-BY-SA DRISS (Updated Oct2019)[creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png)
