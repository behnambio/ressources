<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by Behnam
- Updated Feb2021 </small>

### Modeling of Systemic Autoimmune Disease based on Gene Expression

## Introduction
<small> Systemic autoimmune disease (SAD) develops as a loss of self-tolerance to ubiquitous autoantigens at the systemic level. The resulting diseases are heterogeneous and vary significantly among patients. Unfortunately, such diseases are so complex that not only no single treatment has shown to be curative, but also the currently available symptomatic treatments may not even result in similar responses among different patients. Fatigue, fever, Joint pain, and skin problems are the most common complications of these diseases, which indeed affect the patients’ quality of life and bring about economic crises for both patients and the society. Such conditions are threatening many people’s lives and resources worldwide.
Rapidly developing technologies to characterize genomic and epigenomic variation across individuals enable personalized “precision” approaches to medicine. Personalized medicine is a new paradigm, in which individuals are treated tailored to their genomic and epigenomic variation hoping to find the most effective clinical treatment regimen via deciphering the predictive biomarkers.
Human transcriptome profiles typically contain gene expression values for many thousands of genes, thus representing points in a high-dimensional feature space. A principal technical challenge in characterizing inter-individual variation is to reduce this high dimensionality while preserving biologically relevant inter-individual variation. A second important goal (that may be aligned with the first one) is interpretability of the lower-dimensional variation in the context of other existing biological knowledge, such as molecular pathways or the more general gene interaction networks.


## Datasets
<small> There exists a large number of publicly available datasets in Gene Expression Omnibus (GEO) for various types of autoimmune disease. In this project, we shall employ computational methods to combine such datasets.


## Methodology
<small> In this project, we aim to develop computational approaches in the context of SADs for identifying the (possibly heterogeneous) underlying associated pathways. To meet this end, we will use/develop dimensionality reduction techniques that map available transcriptome profiles into a common and semantically rich low-dimensional space. Dimensionality reduction is to map the input features space to a lower dimensional space using linear or nonlinear transformations. This can be achieved by gene module detection techniques to cluster together the related genes (e.g. based on co-expression networks). Based on this reduced dimensionality space, we will propose a map called Autoimmune map, which brings about new opportunities to better understand the mechanisms underlying SADs, and to stratify autoimmune patients according to their different molecular subtypes.

## working environment
<small> We need to use HPC.

## results
<small> Each axes of the Autoimmune map potentially represents the activity of a set of pathways. Once the transcriptomes of SAD patients have been projected into a low-dimensional space, data samples can be stratified into sub-populations in order to better understand the biology of heterogeneity. It is worth noting that this patient stratification is at the molecular level and may be independent of the disease symptoms and phenotypes. Furthermore, one can test for possible associations of the low-dimensional points with genotypes, which requires additional data recordings. The understanding of patient heterogeneity may lead to tailored treatments, and increase the quality of life of SAD patients. 

# test
> a = 1 
> print(a)

```c
void main (void){
    int a;
    a = 1;
    printf("Hello")
}
```

```R
a = 1
print(a)
```
