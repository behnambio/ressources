# Algorithm

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated Feb2021Ó</small>

----

## Keep in mind : You don't need a computer to write an algorithm !

---
## Algorithms

<center>
![Algorithm](http://www.myconfinedspace.com/wp-content/uploads/2013/08/algorithm.png)
</center>

---

<center>
![Algorithm_xkcd](https://imgs.xkcd.com/comics/algorithms.png)
</center>

---

<center>
Data = ingredient
Algorithm = receipe
</center>

---
### An example : active learning algorithm

<center>
![active learning](https://image.slidesharecdn.com/reinforcementlearninginalgorithmicexecution-bydavidfellahheadoftheemealinearquantresearchgroupatj-161112031453/95/active-learning-in-trading-algorithms-by-david-fellah-head-of-the-emea-linear-quant-research-group-at-jp-morgan-30-638.jpg)
</center>

---

## Descriptive vs programming language

> programming language : how to give instructions to a machine computer to operate/compute something (cqfd : computer)

---

## compiled versus interpreted

* Visual Basic, Python : interpreted

* C : compiled

<center>
![compiled versus interpreted](https://cdn-images-1.medium.com/max/1200/1*3Iy-ohRRXj3lChmEbQTxIQ.png)
</center>

---

## Programming language : characteristics

* A grammar (boolean logic)
* A specific syntax
* Specific words: performative instructions

---
## World Wide Web : specific organizations, norms and governance

* PEP (Python enhancement Proposals)
* W3C


