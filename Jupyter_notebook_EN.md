# Jupyter Notebook and Virtual Environment

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by DRISS
- Updated March2020</small>

Jupyter Notebook has been developped in 2001 with the name ipython notebook and then renamed to Jupyter Notebook in 2014.


> The name “Jupyter” is a strong reference to Galileo, who detailed his discovery of the Moons of Jupiter in his astronomical notebooks.
> The name is also a play on the languages Julia, Python, and R, which are pillars of the modern scientific world.
> Source : [I Python, Your R, We Julia](https://blog.jupyter.org/i-python-you-r-we-julia-baf064ca1fb6)


### Running Jupyter notebook

An easy way is to install Anaconda and then, in you terminal :

user$ jupyter notebook


! Warning ! For Windows user, be sure to use the anaconda prompt

All the informations are [here](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html)

- You can run Julia, Python and R in the same Notebook because there is specific libraries for that. See this [article](https://blog.jupyter.org/i-python-you-r-we-julia-baf064ca1fb6).
- You can choose also to work on python 2 or 3

---

### Add a new file or upload

It will open your web browser and you will see the folder where you type the instruction and your terminal.
You can add a new file, it will be a ipynb file or you can upload one.

---
### Use a Jupyter Notebook :


Different options :
- code
- markdwon
- headline

You just need to click on `run`to execute it

It is really useful for a graphic to display your vizualisation in the jupyter notebook

In Jupyter Notebook, if you run and there is an error, you will get the same trackback than in your terminal.

Between the cell you can have access to the value

When you are happy with your code you can `run all`

---

When your kernel is still processing you have an `Ìn [*]`
If there is a number it is the number of execution `[21]`means that it has been running 21 times



### Cool

- You don't need to load external images
- You can let people play with the paramaters
- Cool for reproductibility in research
- Prepare easily for articles if you write what you are doing (for the methodological part)

### Explanations of a script :

Examples with : https://github.com/c24b/openscience4S/blob/master/Cartographie%20des%20comportements%20des%20electeurs.ipynb


- first import the different modules that you need and comment the script
- import your file (important to understand relative and absolute path)
- You can first show the different variables in your data set (and explain it in markdown to document it)
- All the script is documenting with `#`


### How to use R in Jupyter Notebook

Read the [documentation !](http://docs.anaconda.com/anaconda/user-guide/tasks/use-r-language/)

> tips : grep bash command : grep, which stands for "global regular expression print," processes text line by line and prints any lines which match a specified pattern.

- install a virtual environment with R essentiels inside.
- You can go to "Anaconda Navigator" and see the different environment available and choose the one with r installed.
----


## Virtual environment

Inside your working directory, you can install a virtual environment, which is great if you are using different packages, or python version for several projects. ANd for reproductibilty it is better to know which modules are installed

In your terminal:


```user$ virtualenv -p usr/bin/python3.6 .venv_ipython
``µM
In case your are using anaconda:
```
user$ conda create venv_ipython python=3.6
```

- You can create a virtual environment
- You will need to activate it:

```
user$ source.venv_ipython/bin/activation
```
- After you can install specific packages in your virtual environment

```
pip install networkx
```

- `freeze`make the snapchot of all the packages that your are using:

```
pip freeze
````
- You can create a file requirements:

```
pip freeze >> requirements.txt
```
- The command to quit the virtual env is `desactivate`

---
If I want to run your project, I just have to create a virtual environment

```$virtualenv -p /usr/bin/python3.6 new_env````

- You can mention the version of python you are using

- then activate it

```
$source new_env/bin/activate
```

---

then you can install all the requirements directly

pip install -r requirements.tct


----
and now you can go to your jupyter notebook

- Be sure to launch your jupyter notebook inside your virtual environment

---

## Resources :

https://www.fun-mooc.fr/courses/course-v1:inria+41016+session01bis/about#

https://reproducible-science-curriculum.github.io/workshop-RR-Jupyter/
