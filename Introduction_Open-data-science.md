# Digital technologies, Science and Society : an introduction to open and data science !

<small> Licence CC-BY-SA ![creative commons](https://licensebuttons.net/l/by-sa/3.0/88x31.png) by Célya Gruson-Daniel from DRISS
- Updates 21/02/11</small>

---

## What is open and data science for you ? 

### Open Science ? 

### Data science ? 


----

## Open science : from activism to news norms in research 

### At the beginning : open access activism 

- Budapest Declaration - 2002
- Berlin Declaration - 2003


> Ouvrir la Science. 2019. “Un historique du libre accès aux publications et aux données.” 2019. https://www.ouvrirlascience.fr/un-historique-du-libre-acces-aux-publications-scientifiques-et-aux-donnees.


![Aaron Swartz](https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Aaron_Swartz_profile.jpg/173px-Aaron_Swartz_profile.jpg)

cf [The internet's own boy](https://en.wikipedia.org/wiki/The_Internet%27s_Own_Boy)

==> *Open Science and Open culture are intertwined even if today the value of openess and socio-political conception (libertarianism, commons) tends to be on the sidelines* When a counter-culture become the dominant culture. Turner, Fred. 2010. 

> From Counterculture to Cyberculture: Stewart Brand, the Whole Earth Network, and the Rise of Digital Utopianism. Chicago: University of Chicago Press. http://public.eblib.com/choice/publicfullrecord.aspx?p=602624.


---
### Open Access

Do you need a brief summary ? 

> Ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation, and Bernard Larrouturou. n.d. “Passport for Open Science – A Practical Guide for PhD Students.” Accessed February 7, 2021. https://www.ouvrirlascience.fr/passport-for-open-science-a-practical-guide-for-phd-students.


### From Open access to data management 

 FAIR principles and Data Management Plan (DMP)

![Fair](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/FAIR_data_principles.jpg/800px-FAIR_data_principles.jpg)



---- 

### The growing of institutional implication and management

- 2016 : Europe H2020 "Open innovation, Open Science, Open to the world"
- 2018 : Comité pour la science ouverte France



----
## Today : when you will hear about open science... 


https://www.fosteropenscience.eu/taxonomy/term/121


![Open science](https://upload.wikimedia.org/wikipedia/commons/9/9c/Open_Science_-_Prinzipien.png)

---


![Open Science](https://pbs.twimg.com/media/Dj50Jm7XsAAp5Hg.jpg)


---

### Open Science : 

- Open Access : Plan S, APC, double pay, HAL,  

- Pre-print, pre-registration

- Data sharing, data management plan and open research data 

---

### The reason ? 

- Transparency and reproducibility

![difference](https://rrcns.readthedocs.io/en/latest/_images/reproducibility_spectrum.png)

![Science goes wrong](https://marketingthema.files.wordpress.com/2018/06/20141117sfnnewsreproducability.jpg?w=584)


> Randall D. et al., 2018, [The irreproducibility crisis of modern science: causes, consequences, and the road to reform](https://www.nas.org/reports/the-irreproducibility-crisis-of-modern-science).

----

### Science is broken :  Peer review today

![Peer review](https://i.pinimg.com/originals/73/28/f5/7328f5a01382e286aa68eb00a23cfd7a.gif)

---

### Ethical and legal issues 

Ouvert autant que possible, fermé autant que nécessaire

as open as possible and as closed as necessary 

cf. presentation tomorrow

---
## But you will less hear about 


### Participatory science : decolonizing science 

![Neutrality](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Wikip%C3%A9dia_et_l%27expertise_02.JPG/680px-Wikip%C3%A9dia_et_l%27expertise_02.JPG)

---

### Open source and open science 

> cf. Vos, Rutger, and Pedro Fernandes. 2017. Open Science Open Data Open Source. 21th Century Research Skills for Life Sciences. Zenodo. https://doi.org/10.5281/ZENODO.1015288.

----

### Governance and commons 

- Digital commons
- sustainability and mutualization of infrastructure research 

--- 

### Cognitive justice / Knowledge and Power

---

### Open washing 

![Researchgate](https://blogs.library.ucsf.edu/inplainsight/files/2013/08/ResearchGate.jpg)


----
## It's your turn 


**First instruction :**
  - create an account to framagit
  - send me an email when it's done (wait a few minutes to be added to the groups)
  - find the `2021_syllabus.md`and the `2021_Day1_framework.md`

Well done !

- Create a repo (not a file) in the students_project with your initial and a keyword describing your PhD for example `CGD_open-science-conception`
- Create a file inside named `2021-02-11_test.md`

Answer to this question inside
- What is framagit : how is it organized ? What are the features ? How the files are organized ? What is it used for ?
- What is the language used inside the file .md ?
- Bonus : Why the password of GatherTown is BudapestDeclaration ?

---

## Ressources 

A good way to start 

FOSTER EU. n.d. “FOSTER Open Science.” Accessed February 7, 2021. https://www.fosteropenscience.eu/.
Ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation, and Bernard Larrouturou. n.d. “Passeport pour la Science Ouverte | Guide pratique à l’usage des doctorants.” Accessed February 7, 2021a. https://www.ouvrirlascience.fr/passeport-pour-la-science-ouverte-guide-pratique-a-lusage-des-doctorants.
———. n.d. “Passport for Open Science – A Practical Guide for PhD Students.” Accessed February 7, 2021b. https://www.ouvrirlascience.fr/passport-for-open-science-a-practical-guide-for-phd-students.
OpenScienceMooc. n.d. “Homepage Open Science MOOC.” Open Science MOOC. Accessed February 7, 2021. https://opensciencemooc.eu/.
Vos, Rutger, and Pedro Fernandes. 2017. Open Science Open Data Open Source. 21th Century Research Skills for Life Sciences. Zenodo. https://doi.org/10.5281/ZENODO.1015288.


